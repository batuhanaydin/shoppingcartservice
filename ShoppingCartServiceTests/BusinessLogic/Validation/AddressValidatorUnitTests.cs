using Xunit;

namespace ShoppingCartServiceTests.BusinessLogic.Validation
{
    public class AddressValidatorUnitTests
    {
        [Fact]
        public void IsValid_doesNotHaveCountry_returnFalse()
        {
        }

        [Fact]
        public void IsValid_doesNotHaveCity_returnFalse()
        {
        }

        [Fact]
        public void IsValid_doesNotHaveStreet_returnFalse()
        {
        }

        [Fact]
        public void IsValid_validValues_returnTrue()
        {
        }
    }
}
